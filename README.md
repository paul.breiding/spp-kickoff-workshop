# SPP-Kickoff-Workshop



## Topics
* Commutative algebra
* Lattice Points
* Matroids 
* Statistics 
* Physics 
* Optimization
* Convexity
* Enumerative Combinatorics
* Dynkin Diagrams

## Potential Speakers

Garcon, Kühne, Geiger, Codenotti, Michalek, Portakal,
Baur, Avverkov, Amendola, Winter Vallentin, de Wolff, Nill, Sinn

## Schedule

#### Wednesday
| Time          |      Activity  |  
|----------     |:--------------:|
| 8:45 - 9:00   | Welcome        | 
| 9:00 - 10:00  | Plenary Talk   |   
| 10:00 - 10:30 | Short Talk   |    
| 10:30 - 11:00 | Coffee Break | 
| 11:00 - 11:30 | Short Talk   | 
| 11:30 - 12:00 | Short Talk   | 
| 12:00 - 14:30 | Lunch Break   | 
| 14:30 - 16:00 | Open Table Session   | 
| 16:00 - 16:30 | Coffee Break   | 
| 16:30 - 18:00 | Evaluation   | 

#### Thursday
| Time          |      Activity  |  
|----------     |:--------------:| 
| 9:00 - 10:30 | Open Table Session   | 
| 10:30 - 11:00 | Coffee Break   | 
| 11:00 - 12:30 | Evaluation   | 
| 12:30 - 14:30  | Lunch Break   |   
| 14:30 - 15:30 | Presentation of the SPP   |    
| 15:30 - 16:00 | Coffee Break | 
| 16:00 - 16:30 | Short Talk   | 
| 16:30 - 17:00 | Short Talk   | 
| 17:00 - 17:30 | Short Talk   | 


#### Friday
| Time          |      Activity  |  
|----------     |:--------------:|
| 9:00 - 10:00  | Plenary Talk   |   
| 10:00 - 10:30 | Short Talk   |    
| 10:30 - 11:00 | Coffee Break | 
| 11:00 - 11:30 | Short Talk   | 
| 11:30 - 12:00 | Short Talk   | 


## Open Table Questions
* What problems should be attacked?
* What are the connections of the people a the table to the topics of the SPP?
* What are the connection of the individual projects to the topic of the table?
* What are favorable activities in the context of the SPP? And which of those would you want to organize?
